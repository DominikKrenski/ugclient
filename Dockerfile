FROM nginx:1.21.6

RUN rm /usr/share/nginx/html/* && \
    rm /etc/nginx/conf.d/default.conf && \
    rm /etc/nginx/nginx.conf

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY dist/ /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT [ "nginx", "-g", "daemon off;" ]
