import HomeComponent from './components/home/HomeComponent';

import './App.scss';

const App = () => {
  return (
    <div id='app' className='columns is-multiline'>
      <HomeComponent />
    </div>
  )
}

export default App;
