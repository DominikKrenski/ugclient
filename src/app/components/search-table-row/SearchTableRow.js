import './SearchTableRow.scss';

const SearchTableRow = props => {
  const { data } = props;

  return (
    <tr>
      <td>{data.id}</td>
      <td>{data.name}</td>
      <td>{data.accountDate}</td>
      <td>{data.priceUSD}</td>
      <td>{data.pricePLN}</td>
      <td>{data.createdAt}</td>
      <td>{data.updatedAt}</td>
    </tr>
  )
}

export default SearchTableRow;
