import SearchTableRow from '../search-table-row/SearchTableRow';

import './SearchTable.scss';

const SearchTable = props => {
  const { comps, closeCallback } = props;

  const rows = comps.map((item, i) => {
    return (
      <SearchTableRow
        key={item.id}
        data={item} />
    )
  });

  return (
    <div id='search-table' className='modal is-active'>
      <div className='modal-background'></div>
      <div className='modal-content' id='search-modal-content'>
        <table className='table is-striped is-fullwidth is-narrow'>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nazwa</th>
              <th>Data księgowania</th>
              <th>Cena USD</th>
              <th>Cena PLN</th>
              <th>Data utworzenia</th>
              <th>Ostatnia modyfikacja</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>

      <button
        className='modal-close is-large'
        aria-label='close'
        onClick={closeCallback}
      >
      </button>
    </div>
  )
}

export default SearchTable;
