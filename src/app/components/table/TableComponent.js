import TableRow from '../table-row/TableRow';

import './TableComponent.scss';

const TableComponent = props => {
  const { comps } = props;

  const rows = comps.map((item, i) => {
    return (
      <TableRow
        key={item.id}
        data={item} />
    )
  });

  return (
    <div id='data-table'>
      <table className='table is-striped is-fullwidth is-narrow'>
        <thead>
          <tr>
            <th>ID</th>
            <th>Nazwa</th>
            <th>Data księgowania</th>
            <th>Cena USD</th>
            <th>Cena PLN</th>
          </tr>
        </thead>
        <tbody>
          { rows }
        </tbody>
      </table>
    </div>
  )
}

export default TableComponent;
