import { useEffect, useState } from 'react';

import TableComponent from '../table/TableComponent';
import ComputerForm from '../form/FormComponent';
import SearchTable from '../search-table/SearchTable';

import httpClient from '../../utils/HttpClient';

import './HomeComponent.scss';

const HomeComponent = () => {
  const nameDescending = "nameDescending";
  const nameAscending = "nameAscending";
  const dateDescending = 'dateDescending';
  const dateAscending = 'dateAscending';

  const [data, setData] = useState([]);  // stores all data fetched from server
  const [searchData, setSearchData] = useState([]); // stores data that was finded when searching by name/account date
  const [currentSort, setCurrentSort] = useState(nameAscending); // current sort method
  const [formVisible, setFormVisible] = useState(false); // stores info if add form should be displayed
  const [searchFormValue, setSearchFormValue] = useState({ computerName: '', computerDate: '' }); // stores current values of search forms
  const [searchTableVisible, setSearchTableVisible] = useState(false);  // stores info if table with search results should be displayed

  /**
   * Initial data fetching to populate main table
   */
  useEffect(() => {
    (async () => {
      try {
        const res = await httpClient.get('/computers');

        if (res.data.length > 0) {
          setData(sort(currentSort, res.data));
        }
      } catch (err) {
        console.error(err.data);
      }
    })();
  }, []);

  /**
   * Sets current value of search form elements
   *
   * @param {SyntheticEvent} e
   */
  const handleSearchFormChange = e => {
    const { name, value } = e.target;

    setSearchFormValue({
      ...searchFormValue,
      [name]: value
    });
  }

  /**
   * Starts searching for elements with given name
   *
   * @param {SyntheticEvent} e
   */
  const handleFindByNameButtonClick = async e => {
    e.preventDefault();

    try {
      const res = await httpClient.get(`/computers?name=${searchFormValue.computerName}`);
      setSearchData(res.data);
      setSearchTableVisible(true);
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * Starts searching for elements with given account date
   *
   * @param {SyntheticEvent} e
   */
  const handleFindByDateButtonClick = async e => {
    e.preventDefault();

    try {
      const date = searchFormValue.computerDate.split('-').reverse().join('-');
      const res = await httpClient.get(`/computers?accountDate=${date}`);
      setSearchData(res.data);
      setSearchTableVisible(true);
    } catch (err) {
      console.error(err);
    }
  }


  /**
   * Handles sort method change
   *
   * @param {SyntheticEvent} e
   */
  const handleSortChange = e => {
    const sorted = sort(e.target.value, data);
    setCurrentSort(e.target.value);
    setData(sorted);
  }

  /**
   * Displays form for adding new computer
   */
  const handleAddButtonClick = () => {
    setFormVisible(true);
  }

  /**
   * Closes form
   */
  const closeModal = () => {
    setFormVisible(false);
  }

  /**
   * Closes table with search results
   */
  const closeSearchTable = () => {
    setSearchTableVisible(false);
    setSearchFormValue({ computerName: '', computerDate: '' });
  }

  /**
   * Invoked when new computer was successfully added. Fetching updated data from server.
   */
  const reloadData = async () => {
    setFormVisible(false);

    try {
      const res = await httpClient.get('/computers');

      if (res.data.length > 0) {
        setData(sort(currentSort, res.data));
      }
    } catch (err) {
      console.error(err);
    }

  }

  /**
   * Helper function for sorting elements by selected criteria
   *
   * @param {string} order
   * @param {any[]} dataArray
   * @returns
   */
  const sort = (order, dataArray) => {
    switch (order) {
      case nameAscending:
        return dataArray.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
      case nameDescending:
        return dataArray.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase())).reverse();
      case dateAscending:
        return dataArray.sort((a, b) => {
          const aDate = new Date(a.accountDate.split('-').reverse().join('-'));
          const bDate = new Date(b.accountDate.split('-').reverse().join('-'));
          return aDate - bDate;
        });
      case dateDescending:
        return dataArray.sort((a, b) => {
          const aDate = new Date(a.accountDate.split('-').reverse().join('-'));
          const bDate = new Date(b.accountDate.split('-').reverse().join('-'));
          return bDate - aDate;
        });
    }
  }


  return (
    <div id='home' className='column is-fullwidth'>
      <section className='hero is-small is-primary'>
        <div className='hero-body'>
          <p className='title'>
            Projekt testowy dla Uniwersytetu Gdańskiego
          </p>
        </div>
      </section>

      <div id='content-wrapper' className='columns'>
        <div id='data-fragment' className='column is-9'>
          <TableComponent comps={data} />
        </div>

        <div id='button-fragment' className='column is-3 is-flex is-flex-direction-column'>
          <div>
            <input
              type="radio"
              value={nameAscending}
              checked={currentSort === nameAscending ? true : false}
              name="sort"
              onChange={handleSortChange}
            />
            <label htmlFor={nameAscending}> Sortuj według nazwy (rosnąco)</label>
          </div>

          <div>
            <input
              type="radio"
              value={nameDescending}
              checked={currentSort === nameDescending ? true : false}
              name="sort"
              onChange={handleSortChange}
            />
            <label htmlFor={nameDescending}> Sortuj według nazwy (malejąco)</label>
          </div>

          <div>
            <input
              type="radio"
              value={dateAscending}
              checked={currentSort === dateAscending ? true : false}
              name="sort"
              onChange={handleSortChange}
            />
            <label htmlFor={dateAscending}> Sortuj według daty (rosnąco)</label>
          </div>

          <div>
            <input
              type="radio"
              value={dateDescending}
              checked={currentSort === dateDescending ? true : false}
              name="sort"
              onChange={handleSortChange}
            />
            <label htmlFor={dateDescending}> Sortuj według daty (malejąco)</label>
          </div>

          <div id='form-name-wrapper'>
            <form noValidate={true} onSubmit={handleFindByNameButtonClick}>
              <div className='field has-addons'>
                <div className='control'>
                  <input
                    className='input'
                    type='text'
                    placeholder='Szukaj wg nazwy'
                    name='computerName'
                    value={searchFormValue.computerName}
                    onChange={handleSearchFormChange}
                  />
                </div>
                <div className='control'>
                  <input className='input is-button is-success' type='submit' value='Szukaj' name='computerName' />
                </div>
              </div>
            </form>
          </div>

          <div id='form-date-wrapper'>
            <form noValidate={true} onSubmit={handleFindByDateButtonClick}>
              <div className='field has-addons'>
                <div className='control is-fullwidth'>
                  <input
                    className='input'
                    type='date'
                    name='computerDate'
                    value={searchFormValue.computerDate}
                    onChange={handleSearchFormChange}
                  />
                </div>
                <div className='control'>
                  <input className='input is-button is-success' type='submit' value='Szukaj' />
                </div>
              </div>
            </form>
          </div>

          <div id='add-button'>
            <button
              className='button is-success is-rounded'
              onClick={handleAddButtonClick}
            >
              Dodaj komputer
            </button>
          </div>
        </div>
      </div>

      {
        formVisible &&
        <ComputerForm closeModalCallback={closeModal} successCallback={reloadData} />
      }

      {
        searchTableVisible &&
        <SearchTable closeCallback={closeSearchTable} comps={searchData} />
      }
    </div>
  )
}

export default HomeComponent;
