import './TableRow.scss';

const TableRow = props => {
  const { data } = props;

  return (
    <tr>
      <td>{data.id} </td>
      <td>{data.name}</td>
      <td>{data.accountDate}</td>
      <td>{data.priceUSD}</td>
      <td>{data.pricePLN}</td>
    </tr>
  )
}

export default TableRow;
