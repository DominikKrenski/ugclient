import { useState } from 'react';

import httpClient from '../../utils/HttpClient';

import './FormComponent.scss';

const ComputerForm = props => {
  const { closeModalCallback, successCallback } = props;

  const nbpURL = 'http://api.nbp.pl/api/exchangerates/rates/c/usd';

  const initialValues = {
    name: '',
    accountDate: '',
    priceUSD: ''
  }

  const [values, setValues] = useState(initialValues);

  const handleInputChange = e => {
    const { name, value } = e.target;
    console.log(name + ' ' + value);

    setValues({
      ...values,
      [name]: value
    });
  }

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      const res = await httpClient.get(`${nbpURL}/${values.accountDate}/?format=json`);
      console.log(res.data.rates[0].ask);

      // prepare data for server
      const data = {
        name: values.name,
        accountDate: values.accountDate.split('-').reverse().join('-'),
        priceUSD: Number(values.priceUSD).toFixed(3),
        pricePLN: (Number(values.priceUSD) * res.data.rates[0].ask).toFixed(3)
      }

      await httpClient.post('/computers', data);

      successCallback();
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <div
      id='computer-form-wrapper'
      className='modal is-active'
    >
      <div className='modal-background'></div>
      <div className='modal-content'>
        <form id='computer-form' noValidate={true} onSubmit={handleSubmit}>

          <div className='field'>
            <label className='label'>Nazwa</label>
            <div className='control'>
              <input
                className='input'
                type='text'
                name='name'
                value={values.name}
                onChange={handleInputChange}
              />
            </div>
          </div>

          <div className='field'>
            <label className='label'>Data księgowania</label>
            <div className='control'>
              <input
                className='input'
                type='date'
                name='accountDate'
                value={values.accountDate}
                onChange={handleInputChange}
              />
            </div>
          </div>

          <div className='field'>
            <label className='label'>Cena USD</label>
            <div className='control'>
              <input
                className='input'
                type='number'
                name='priceUSD'
                value={values.priceUSD}
                onChange={handleInputChange}
              />
            </div>
          </div>

          <div className='field' id='submit-button-wrapper'>
            <div className='control'>
              <input type='submit' className='button is-success' value='Dodaj' />
            </div>
          </div>
        </form>
      </div>

      <button
        className='modal-close is-large'
        aria-label='close'
        onClick={closeModalCallback}
      >
      </button>
    </div>
  )
}

export default ComputerForm;
